package com.shantihouse.api;

public class Greeting {

    private final long systemId;
    private String idNumber;
    private String firstName;
    private String lastName;
    private String gender;
    private  String generalMessage;
    private String approval;

    public Greeting(long systemId, String idNumber, String firstName,String lastName, String gender, String message, String approval){
        this.systemId = systemId;
        this.idNumber = idNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.generalMessage = message;
        this.approval = approval;

    }

    public long getSystemId(){return this.systemId;}

    public String getIdNumber(){return this.idNumber;}

    public String getfirstName(){return this.firstName;}

    public String getLastName(){return this.lastName;}

    public String getGender(){return this.gender;}

    public String getGeneralMessage(){return this.generalMessage;}

    public String getApproval(){return this.approval;}

}


