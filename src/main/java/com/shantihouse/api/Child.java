package com.shantihouse.api;

public class Child {

    public enum Gender {
        Female,
        Male
    }

    public enum House {
        Tel_aviv,
        Bamidbar,
        GeneralHouse
    }

    private final long systemId;
    private String idNumber;
    private String firstName;
    private String lastName;
    private Gender gender;
    private String motherName;
    private String fatherName;
    private House house;

    public Child(long systemId, String idNumber, String firstName, String lastName, String gender, String motherName, String fatherName, String house) {
        this.systemId = systemId;
        this.idNumber = idNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = Gender.valueOf(gender);
        this.motherName = motherName;
        this.fatherName = fatherName;
        this.house = House.valueOf(house);

    }

    public long getSystemId() {
        return this.systemId;
    }

    public String getIdNumber() {
        return this.idNumber;
    }
    public void setIdNumber(String newId) {
        this.idNumber = newId;
    }

    public String getfirstName() {
        return this.firstName;
    }
    public void setfirstName(String newFirstName) {
        this.firstName = newFirstName;
    }

    public String getLastName() {
        return this.lastName;
    }
    public void setLastName(String newLastName) {
        this.lastName = newLastName;
    }

    public String getGender() {
        return this.gender.toString();
    }
    public void setGender(String newGender) {
        this.gender = Gender.valueOf(newGender);
    }

    public String gethouse() {
        return this.house.toString();
    }
    public void setHouse(String newHouse) {
        this.house = House.valueOf(newHouse);
    }

    public String getMotherName() {
        return this.motherName;
    }
    public void setMotherName(String newMotherName) {
        this.motherName = newMotherName;
    }

    public String getFatherName() {
        return this.fatherName;
    }
    public void getFatherName(String newFatherName) {
        this.fatherName = newFatherName;
    }

}



